/*

1. In the s23 folder, create an a1 and an index.html and index.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
6. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.
8. Create a constructor for creating a pokemon with the following properties:
- Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)
9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
13. Invoke the tackle method of one pokemon object to see if it works as intended.
14. Create a git repository named S23.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.
17. Send an ss of your work

*/

console.log("Hello World!");

const trainer = {};
trainer.name = "Ash Ketchum";
trainer.age = 12;
trainer.pokemons = ["Pikachu", "Squirtle", "Charmander", "Bulbasaur"];
trainer.friends = {
	hoenn: ["May", "Max"],
	kanto: ['Misty', 'Brock']
}

trainer.talk = function(){
			console.log("Pikachu! I choose you!");
		}

console.log(trainer);
console.log("Results of dot notation:\n",trainer.name);		

console.log("Results of square bracket notation:\n",trainer['pokemons']);	

trainer.talk();		

function Pokemon(name,level){

		this.name = name;
		this.level = level;
		this.health = level * 2;
		this.attack = level * 1.5;
		this.tackle = function(target){
			console.log(this.name + " used tackle on " + target.name);

			//Mini Activity (5 min, 8:34PM)
				
				//reduce the target object's health property by subtracting and reassining its value based on the pokemon's attack
				//target.health = target.health - this.attack
				target.health -= this.attack

				//example if health is not less than 0
					//rattata's health is now reduced to 5

				console.log(target.name + "'s health is now reduced to " + target.health);

				//if the target's health is less than or equal to 0 we will invoke the faint method, otherwise printout the pokemon's new health

				if(target.health<=0){
					target.faint()
				}

		}

		//create another method called faint
		this.faint = function(){
			console.log(this.name + " fainted. Tackle is super effective");
		}

	}

	let pikachu = new Pokemon ("Pikachu", 12);
	let geodude = new Pokemon ("Geodude", 8);
	let mewtwo= new Pokemon ("Mewtwo", 100);
	let archeus = new Pokemon ("Archeus", 102);
	
	
	console.log(pikachu);
	console.log(geodude);
	console.log(mewtwo);
	console.log(archeus);
	
	geodude.tackle(pikachu);	
	console.log(pikachu);

	pikachu.tackle(geodude);
	console.log(geodude);

	mewtwo.tackle(archeus);
	console.log(archeus);

	archeus.tackle(mewtwo);
	console.log(mewtwo);